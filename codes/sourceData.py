#! /Users/biao/anaconda/bin python


import glob, json, sys

#path = sys.argv[1]
#output_file = sys.argv[3]

def combineJsonFiles(path, output_file):
    result = []
    
    for f in glob.glob(path):
        with open(f, 'rb') as infile:
            result.extend(json.load(infile))
    
    with open(output_file, 'wb') as outfile:
        json.dump(result, outfile)
        
        
        
    
combineJsonFiles('scrape_DS_profiles/Aaron_Sander*.json', 'data/DS_data.json')    