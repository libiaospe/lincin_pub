#! /Users/biao/anaconda/bin python

'''
Scrape linkedin public profile
'''

from bs4 import BeautifulSoup
from selenium import webdriver
import sys, collections, argparse
import time, json, os, logging


class LinkedInPub:
    
    '''
    Functionalities to scrape linkedin public profiles
    '''
    def __init__(self, f_visited_links, f_unvisited_links, job_title_keyword='data scientist', output_prefix='result'):
        
        self.job_title_keyword = job_title_keyword
        self.output_prefix = output_prefix
        self.f_visited_links = f_visited_links
        self.f_unvisited_links = f_unvisited_links
        
        # read files of visited links and unvisited links
        self.visited_links = json.load(open(f_visited_links, 'r')) if os.path.exists(f_visited_links) else []
        
        try:
            self.unvisited_links = json.load(open(f_unvisited_links, 'r'))
        except:
            raise ValueError("Need to specify profiles to begin with and save link(s) to {}".format(f_univisted_links))
        
        # list of dicts to save profiles to be scraped
        self.profile_dicts = []
        
        # start time
        self.time_start = time.time()
        
        # counter
        self.counter = 0
        return
    
    
    @staticmethod
    def _soupWithHTML(url):
        '''
        return html and soup
        '''
        try:
            #display = Display(visible=0, size=(1024, 768))
            #display.start()
            # establish the web server 
            driver = webdriver.Firefox() 
            driver.get(url)
            
            html = repr(driver.page_source)
            soup = BeautifulSoup(html, 'lxml') #specify parser or it will auto-select for you
            
            # quite the web server
            driver.quit()
            
            return html, soup
            
        except Exception, e:
            print e
            return None, None
        
    
    def worker(self, max_num=50):
        '''
        worker to scrape up to 'max_num' profiles each time, update libraries and I/O
        '''
        # recursively process top profile from self.unvisited_links until reaching max_num
        
        if len(self.unvisited_links) == 0:
            raise ValueError("No more unvisited linked left to follow! Quit now\n")
        
        while self.counter < max_num:
            
            # select top unvisited profile if there is any
            if len(self.unvisited_links) > 0:
                top_profile_link = self.unvisited_links[0]
            else:
                break
            
            # scrape
            if self.scrapeProfile(top_profile_link):  # if successful
                self.counter += 1
            
        # output to file in json format
        self.fileOutput()
        return 
        
    
    def fileOutput(self):
        
        # output self.visited_links
        with open(self.f_visited_links, 'w') as fout:
            json.dump(self.visited_links, fout)
        
        # output self.unvisited_links
        with open(self.f_unvisited_links, 'w') as fout:
            json.dump(self.unvisited_links, fout)
        
        # output scraped profiles:
        f = "{}_{}_to_{}.json".format(self.output_prefix,
                                      len(self.visited_links) - len(self.profile_dicts) + 1,
                                      len(self.visited_links))
        with open(f, 'w') as fout:
            json.dump(self.profile_dicts, fout)
        
        return 
        
    
    
    def scrapeProfile(self, profile_link):
        '''
        '''
        print_fields=['experience', 'education', 'skills', 'patents', 'projects', 'publications', 'languages', 'interests', 'volunteering']
        html, soup = LinkedInPub._soupWithHTML(profile_link)
        
        # remove profile link from self.unvisited_links
        self.unvisited_links.remove(profile_link)
        
        if soup is None:
            return False
        
        # add profile_link to self.visited_links
        self.visited_links.append(profile_link)
        
        # parse soup info
        self._soupInfoParser(profile_link, soup, print_fields)
        
        # screen output
        print "Done Scraping {}'s profile, job title is {}. # of scraped profiles = {}; total # of profiles = {}\n".format(self.name, self.title, len(self.profile_dicts), len(self.visited_links))
        
        time_now = time.time()
        t = time_now - self.time_start
        print 'Total time spent: {} mins'.format(round(t / 60, 2))
        print 'Average per profile: {} secs\n'.format(round(t / (self.counter + 1), 1))
        
        # get additional related profiles from the "People Also Viewed" (PAV) section
        self._pavHandler(soup, html)   # updating self.unvisited_links
        
        return True
    
    
    def _pavHandler(self, soup, html):
        '''
        process "People Also Viewed" section
        '''
        tmp = soup.find('div', {'class':'browse-map'})
    
        if tmp is None:   # if people view none
            return
        
        # job_titles
        tmp1 = tmp.find_all('p', {'class':'headline'})
        job_titles = [i.contents[0].lower() for i in tmp1]
        
        # profile_links
        tmp2 = tmp.find_all('a', href=True)
        profile_links = [tmp2[2*idx]['href'] for idx in range(len(tmp2) / 2)]  # note that there are 20 items, 2x dups
        
        # names
        tmp3 = tmp.find_all('div', {'class':'info'})
        names = [i.find('h4', {'class':'item-title'}).findNext("a").contents[0] for i in tmp3]
        
        # query indices of individuals that have desired job title, e.g. data scientist
        indices = []
    
        flag = 0
        for idx, name in enumerate(names):
            
            # check if the profile link has already been visited
            if profile_links[idx] in self.visited_links:
                
                print '{} visited!\n'.format(name)
                continue
            
            # check if this person has a job title
            if '{}</a></h4><p class="headline">'.format(name) not in html:
                flag += 1
                continue
            
            else:  # check if job title satisfies
                try:
                    if self.job_title_keyword in job_titles[idx - flag]:
                        indices.append(idx)
                        
                except:  # error occurs if two people have same name while one has job title but the other does not
                    # ignore whole group
                    return
        
        tmp_links = [profile_links[idx] for idx in indices]
        
        # newly appeared profiles to follow up recursively - save to self.unvisited_links
        self.unvisited_links.extend([l for l in tmp_links if l not in self.unvisited_links])
        
        return
    

    def _soupInfoParser(self, profile_link, soup, fields):
       
        ## topcard information
        # name
        try:
            name = soup.find('div', {'class':'profile-overview-content'}).find('h1', {'id':'name'}).getText()
        except: # 
            print "WARNING! Refused Connection! - Couldn't read name on {}".format(profile_link)
            name = None
        self.name = name
        
        # picture link
        try:
            pic_link = soup.find('img', {'alt':name}).get('src')
        except:
            pic_link = None
        
        # title
        try:
            title = soup.find('p', {"class":"headline title", "data-section":'headline'}).getText()
        except:
            title = None
        self.title = title
        
        # locality
        try:
            locality = soup.find('span', class_='locality').getText()
        except:
            locality = None
            
        # Industry
        try:
            industry = soup.find('dt', text='Industry').findNext("dd").contents[0]
        except:
            industry = None
            
        # current
        try:
            current = soup.find('th', text='Current').findNext("a").contents[0]
        except:
            current = None
            
        # previous
        try:
            previous = soup.find('th', text='Previous').findNext('a').contents[0]
        except:
            previous = None
            
        # Education
        try:
            edu = soup.find('th', text='Education').findNext('a').contents[0]
        except:
            edu = None
            
        ## summary information
        try:
            summary = soup.find('section', {"id": 'summary', "class":"profile-section"}).findNext('p').contents[0]
            assert len(summary) > 0
        except:
            summary = None
        
        # create ordered dict to store information
        info_dict = collections.OrderedDict({
                     'link': profile_link, 
                     'name': name,
                     'pic_link': pic_link,
                     'title': title,
                     'locality': locality,
                     'industry': industry,
                     'current': current,
                     'previous': previous,
                     'edu': edu,
                     'summary': summary})
        
        # other info fields
        for f in fields:
            try:
                info_dict.update({f: soup.find('section', { "id" : f }).getText()}) 
            except:
                info_dict.update({f: None})
        
        # add info_dict to self.profile_dicts and screen output
        self.profile_dicts.append(info_dict)
    
        return


def parser_argument(parser):
    
    parser.add_argument('-v', '--v_file',
                        type=str,
                        required=True,
                        help='''specify path to file of visited links/profiles''')
    
    parser.add_argument('-u', '--u_file',
                        type=str,
                        required=True,
                        help='''specify path to file of unvisited links/profiles''')
    
    parser.add_argument('-j', '--job_title',
                        type=str,
                        default='data scientist',
                        help='''job title keyword(s)''')
    
    parser.add_argument('-o', '--output_prefix',
                        type=str,
                        default='result',
                        help='''specify path to the prefix of output file(s)''')
    
    parser.add_argument('-n', '--num',
                        type=int,
                        default=100,
                        help='''maximum allowed number of profiles to fish per scraping worker''')
    
    parser.add_argument('-r', '--rep',
                        type=int,
                        default=5,
                        help='''number of worker replicates''')
    
    parser.add_argument('--debug',
                        default=False,
                        action='store_true',
                        help=argparse.SUPPRESS)


def main_func(args):
    
    for idx, r in enumerate(range(args.rep)):
        
        print 'Fishing worker # {}:\n'.format(idx + 1)
        
        ins = LinkedInPub(f_visited_links = args.v_file,
                          f_unvisited_links = args.u_file,
                          job_title_keyword = args.job_title,
                          output_prefix = args.output_prefix)
        ins.worker(max_num = args.num)
    
    return
    
    
if __name__ == '__main__':
    
    master_parser = argparse.ArgumentParser(
        description = '''Program to fish Linkedin public profiles''',
        prog = 'linpub',
        epilog = '''Biao Li (libiaospe@gmail.com) (c) 2016. License: GNU General Public License (http://www.gnu.org/license/)'''
    )
    
    master_parser.add_argument('--version', action='version', version='%(prog)s0.0.9-rc1')
    
    parser_argument(master_parser)
    master_parser.set_defaults(func=main_func)
    
    # getting arguments
    args = master_parser.parse_args()
    if args.debug:
        args.func(args)
    else:
        try:
            args.func(args)
        except Exception as e:
            logging.error(e)
            sys.exit('An ERROR has occured: {}'.format(e))
    
    sys.exit()
    
    
    
