from bokeh.charts import Bar, output_file, show
from bokeh.charts.attributes import cat, color
from bokeh.charts.operations import blend
import pandas as pd


def barPlot(data_file, x_axis, y_axis, y_values=[], y_name='', y_labels_name='', cols=[], title='', output='', rename_y={True: 'Yes', False: 'No'}, x_sorted=False):

    df = pd.read_csv(data_file)
    
    if y_axis.startswith('is_'):
        df[y_axis] = df[y_axis[3:]].notnull()
    
    # filter top industries
    
    top_n = 20
    
    tmp = df[x_axis].value_counts(normalize=True).index.tolist()[:top_n]
    
    df = df[df[x_axis].isin(tmp)][[x_axis, y_axis]]
    df.dropna(inplace=True)
    
    # get count of each unique combination of industry and new_title
    tmp = tmp = df.groupby([x_axis, y_axis]).size().reset_index()
    
    # restore into a new df with industry as indices, new_title as columns and counts as elements (fillin nan with 0)
    df = tmp.pivot(index=x_axis, columns=y_axis, values=0).fillna(0)
    
    df.rename(columns=rename_y, inplace=True)
    
    # add new column 'total' count
    df['total'] = df.apply(sum, 1)
    df = df.sort('total', ascending=False)
    
    df.reset_index(level=0, inplace=True)
    
    # plot
    bar = Bar(df,
              values=blend(*y_values, name=y_name, labels_name=y_labels_name),
              label=cat(columns=x_axis, sort=x_sorted),
              stack=cat(columns=y_labels_name, sort=False),
              color=color(columns=y_labels_name, palette=cols,
                          sort=False),
              legend='top_right',
              title=title,
              tooltips=[(y_name, '@{}'.format(y_labels_name)), (x_axis, '@{}'.format(x_axis))])
    
    
    output_file(output, title="stacked_bar.py example")
    
    show(bar)
    
    return 



# bar plot 1 - industry vs title
barPlot('../data/Norm_DS_data.csv', x_axis='industry', y_axis='new_title', y_values=['Junior Data Scientist', 'Data Scientist', 'Senior Data Scientist', 'Principal Data Scientist'], y_name='title', y_labels_name='DS', cols=['SaddleBrown', 'SkyBlue', 'Goldenrod', 'orchid'], title='DS Titles Sorted By Industry', output='../figures/industry_title_bar.html') 


# bar plot 2 - industry vs photo
df = barPlot('../data/Norm_DS_data.csv', x_axis='industry', y_axis='is_pic_link', y_values=['Yes', 'No'], y_name='picture', y_labels_name='pic', cols=['SkyBlue', 'Goldenrod'], title='DS Profiles Having Photos Sorted By Industry', output='../figures/industry_pic_bar.html', rename_y={True: 'Yes', False: 'No'})


# bar plot 3 - industry vs degree
df = barPlot('../data/Norm_DS_data.csv', x_axis='industry', y_axis='degree_level', y_values=['Bachelor', 'Master', 'Ph.D.'], y_name='degree', y_labels_name='deg', cols=['SkyBlue', 'Goldenrod', 'orchid'], title='DS Educational Degree Sorted By Industry', output='../figures/industry_degree_bar.html', rename_y={3: 'Ph.D.', 2: 'Master', 1: 'Bachelor'})

# bar plot 4 - grad_year vs title
barPlot('../data/Norm_DS_data.csv', x_axis='grad_year', y_axis='new_title', y_values=['Junior Data Scientist', 'Data Scientist', 'Senior Data Scientist', 'Principal Data Scientist'], y_name='title', y_labels_name='DS', cols=['SaddleBrown', 'SkyBlue', 'Goldenrod', 'orchid'], title='DS Post-graduation Years Sorted By Industry', output='../figures/gradyear_title_bar.html', x_sorted=True) 


