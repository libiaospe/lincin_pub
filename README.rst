What lincin_pub is

lincin_pub is a Python program that can acquire LinkedIn public profiles on large scale. 

The main feature is to scrape data beginning with any public profile and tracing recursively other profiles that people also viewed with matched job titles. 

The scraped data are saved in JSON format with the following fields: name, summary, title, locality, industry, current position, previous position, education, picture link, url link, experiences, skills, publications, patents, volunteering, etc. 



